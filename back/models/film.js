lsimport mongoose from "mongoose";
const Schema = mongoose.Schema;

const FilmSchema = new Schema({
  Name : {
    type : String
  },
  Author : {
    type : String,
  },
  Sinopsys : {
    type : String,
  },
  Fecha : {
    type : String,
  },
  VimID : {
    type : String,
  },
  TrailerVimID : {
    type : String,
  },
  Poster : {
    type : String,
  }
});


const FilmSchema = mongoose.model('Film',UserSchema);

export default FilmSchema;
