import express from 'express';
import passport from 'passport';
import jwt from 'jsonwebtoken';

var router = express.Router();
// import AdminController from '../controllers/AdminController';
// import auth from '../middlewares/auth';

//When the user sends a post request to this route, passport authenticates the user based on the
//middleware created previously
router.post('/signup', function(req, res, next) {
  passport.authenticate('signup', function(err, user, info) {
    if (err || !user) {
      return res.json({
        message: info
      });
    }
    return res.json({
      message: 'Te has registrado exitosamente'
    });
  })(req, res, next);
});

// router.post('/signup', passport.authenticate('signup', { session : false }) , async (req, res, next) => {
//   res.json({
//     message : 'Se ha creado el registro correctamente'
//   });
// });

router.post('/login', async (req, res, next) => {
  passport.authenticate('login', async (err, user, info) => {
    try {
      if (err || !user) {
        return res.json({
          message: 'Usuario y/o contraseña incorrecta'
        });
      }
      req.login(user, {
        session: false
      }, async (error) => {
        if (error) return next(error)
        //We don't want to store the sensitive information such as the
        //user password in the token so we pick only the email and id
        const body = {
          _id: user._id,
          email: user.email
        };
        //Sign the JWT token and populate the payload with the user email and id
        const token = jwt.sign({
          user: body
        }, 'el_ojo_que_todo_lo_ve', {
          expiresIn: '30m'
        });
        //Send back the token to the user
        return res.json({
          token
        });
      });
    } catch (error) {
      return next(error);
    }
  })(req, res, next);
});



export default router;
