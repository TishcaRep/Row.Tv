import passport from "passport";
import {Strategy as localStrategy } from 'passport-local';
import UserModel  from '../models/auth';
import {Strategy as JWTstrategy } from "passport-jwt";
import {ExtractJwt as ExtractJWT} from 'passport-jwt';

//Create a passport middleware to handle user registration
passport.use('signup', new localStrategy({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback:true

}, async (req,email, password, done) => {
  try {
    //Save the information provided by the user to the the database
    const user = await UserModel.create({
      email,
      password
    });
    //Send the user information to the next middleware
    return done(null, user);
  } catch (error) {
    return done(null, false, {
      message: 'Este correo ya esta registrado'
    });
  }
}));

//Create a passport middleware to handle User login
passport.use('login', new localStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, async (email, password, done) => {
  try {
    //Find the user associated with the email provided by the user
    const user = await UserModel.findOne({
      email
    });
    if (!user) {
      //If the user isn't found in the database, return a message
      return done(null, false, {
        message: 'User not found'
      });
    }
    //Validate password and make sure it matches with the corresponding hash stored in the database
    //If the passwords match, it returns a value of true.
    const validate = await user.isValidPassword(password);
    if (!validate) {
      return done(null, false, {
        message: 'Wrong Password'
      });
    }
    //Send the user information to the next middleware
    return done(null, user, {
      message: 'Logged in Successfully'
    });
  } catch (error) {
    return done(error);
  }
}))

//This verifies that the token sent by the user is valid
try {
  passport.use(new JWTstrategy({
    //secret we used to sign our JWT
    secretOrKey: 'el_ojo_que_todo_lo_ve',
    //we expect the user to send the token as a query parameter with the name 'secret_token'
    // jwtFromRequest : ExtractJWT.fromUrlQueryParameter('token')
    jwtFromRequest: ExtractJWT.fromExtractors([ExtractJWT.fromAuthHeaderAsBearerToken(), ExtractJWT.fromBodyField("token"), ExtractJWT.fromUrlQueryParameter("token")]),
    // secretOrKey: process.env.AUTH_SECRET
  }, async (token, done) => {
    try {
      //Pass the user details to the next middleware
      return done(null, token.user);
    } catch (error) {
      done(error);
    }
  }));
} catch (e) {
  console.log(e);
} finally {

};
