import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import bodyParser from 'body-parser';
import UserModel  from './models/auth';
import './auth/auth';
//TODO Conexión base de datos
const mongoose = require('mongoose');


const options = {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true};

//TODO Or using promises
mongoose.connect("mongodb+srv://row:pass_test@cluster0.cdnin.mongodb.net/Row_Tv", options).then(

  () => { console.log('Conectado a DB') },

  err => { console.log(err) }
);

const app=express();
app.use( bodyParser.urlencoded({ extended : false }) );
// app.use(bodyParser.json({limit: '50mb'}));
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));


app.use(morgan('dev'));
app.use(cors());




// app.use(express.json());
// app.use(express.urlencoded({extended:true}));

//TODO Rutas
import auth from './router/auth';
app.use('/api', auth);


app.set('port', process.env.PORT || 3100);


app.listen(app.get('port'),()=> {
    console.log('server on port ' + app.get('port'));

});
