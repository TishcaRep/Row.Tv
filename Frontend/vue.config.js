module.exports = {
  pwa: {
    name: 'Row Tv',
    themeColor: '#993399',
    msTileColor: '#14182a',
    manifestOptions: {
      background_color: '#14182a'
    }
  }
}