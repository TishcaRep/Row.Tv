import { createRouter, createWebHistory } from "vue-router";
import NotFound from "../views/NotFound.vue";

import Home from "../views/Home.vue";


const routes = [
  {
    path: "/",
    name: "Discover",
    component: Home
  },
  {
    path: "/about",
    name: "Film",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/:catchAll(.*)",
    component: NotFound,
  }

];

const router = createRouter({
  history: createWebHistory(),
  routes
});

router.beforeEach((to,from,next)=>{
  document.title = process.env.VUE_APP_TITLE + " - " + to.name;
  next();
});


export default router;
